import React from "react"
import { useState, useEffect } from 'react';



export default function ActivityImage(props){
    let [isLoading, setIsLoading] = useState(true);
    const [loadedMeetups, setLoadedMeetups] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    fetch(
      'https://api-wefarm-default-rtdb.firebaseio.com/ActivityImage.json'
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const meetups = [];

        for (const key in data) {
          const meetup = {
            id: key,
            ...data[key]
          };

          meetups.push(meetup);
        }
        setIsLoading(false);
        setLoadedMeetups(meetups);
      });
  }, []);
    const arrayImage=[]

    const image = loadedMeetups.map(item => {
        return (
         arrayImage.push(item.image)
        )
    })

    return(
      <ul className="mt-4">
      <li className="mb-4">{props.title}</li>
      <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
          <div className="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={0} className="active" aria-current="true" aria-label="Slide 1" />
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={1} aria-label="Slide 2" />
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={2} aria-label="Slide 3" />
          </div>
          <div className="carousel-inner">
          <div className="carousel-item active">
            <img src={arrayImage[0]} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={arrayImage[1]} className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src={arrayImage[2]} className="d-block w-100" alt="..." />
          </div>
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="visually-hidden">Next</span>
          </button>
      </div>                                           
  </ul> 
    )
}